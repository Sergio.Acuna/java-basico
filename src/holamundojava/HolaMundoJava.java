/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package holamundojava;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class HolaMundoJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //System.out.println("Hola Mundo desde Java");
        int miVariableEntera = 10; //Definimos la variable como enteros
        System.out.println(miVariableEntera);

        //Modificamos el valor de la variable no se vuelve a definir ya esta definida en la linea 19
        miVariableEntera = 5;
        System.out.println(miVariableEntera);

        String miVariableCadena = "Saludos";
        System.out.println(miVariableCadena);

        miVariableCadena = "Adios";
        System.out.println(miVariableCadena);

        //var - Inferencia de tipos Java
        var miVariableEntera2 = 10;
        System.out.println(miVariableEntera2);

        var miVariableCandena2 = "Nueva Cadena";
        System.out.println("miVariableCandena2 = " + miVariableCandena2);

        //Valores permitidos en el nombre de variables
        var miVariable = 1;
        var _miVariable = 2;
        var $miVariable = 3;
        //var áVariable = 10; No se recomienda utilizar
        //var #miVariable = 10; No se permite utilizar caracteres especiales 

        //Concatenación de cadenas 
        var usuario = "Juan";
        var titulo = "Ingeniero";

        var union = titulo + " " + usuario; //union de variables 
        System.out.println("union = " + union);
        
        var i = 3;
        var j = 4;
        
        System.out.println(i + j);//se realiza suma de numeros
        System.out.println(i + j + usuario);//contexto cadena primero realiza la suma y luego la concatenacion
        System.out.println(usuario + i + j);//contexto cadena toma todo como una cadena de izquierda a derecha 
        System.out.println(usuario + (i + j));//contexto cadena suma los números
        System.out.println(usuario + " " +(i + j));//uso de parentesis modifica la prioridad 
        
        //Caracteres especiales en Java
        var nombre = "Karla";//Información en codigo duro no lo ingresa el usuario
        
        System.out.println("Nueva linea: \n" + nombre);//salto de linea
        System.out.println("Tabulador: \t" + nombre);//tabulador
        System.out.println("Retroceso: \b" + nombre);//Retrocede una posición en este caso el espacio antes del \
        System.out.println("Retriceso: \b\b" + nombre);//Retrocede dos posiciones en este caso borraria los puntos
        System.out.println("Comilla simple: \'" + nombre + "\'");//comillas simples
        System.out.println("Comilla doble: \"" + nombre + "\"");//comilla doble se utiliza diagonal inversa para que funcione
    
        //Clase Scanner en Java
        System.out.println("Escribe tu nombre");
        Scanner consola = new Scanner(System.in);//INgresar por consola
        var usuario1 = consola.nextLine();
        System.out.println("usuario = " + usuario1);
        System.out.println("Escribe el titulo");
        var titulo1 = consola.nextLine();
        System.out.println("titulo = " + titulo1); 
        System.out.println("Resultado: " + titulo1 + " " +usuario1);
        
        //Tipos primitivos y tipos enteros 
        
        /*
            tipos primitivos enteros: byte(8 bits), short(16 bits), char(16 bits), int(32 bits), long(64 bits).
 
        */
        
        byte numeroByte = (byte)129;
        System.out.println("Valor byte = " + numeroByte);
        System.out.println("Valor minimo byte" + Byte.MIN_VALUE);
        System.out.println("Valor maximo byte" + Byte.MAX_VALUE);
        
        short numeroShort = (short)32768;//Se obliga al compilador a realizar la conversión
        System.out.println("Valor Short = " + numeroShort);
        System.out.println("Valor minimo short" + Short.MIN_VALUE);
        System.out.println("Valor maximo short" + Short.MAX_VALUE);
        
        int numeroInt = (int)2147483648L;//Con el parentesis no se puede obligar al compilador entonces tenemos que agregar L para volverlo un dato long
        System.out.println("Valor int = " + numeroInt);
        System.out.println("Valor minimo int" + Integer.MIN_VALUE);
        System.out.println("Valor maximo int" + Integer.MAX_VALUE);
        
        long numeroLong = 9223372036854775807L;
        System.out.println("numeroLong = " + numeroLong);
        System.out.println("Valor minimo long" + Long.MIN_VALUE);
        System.out.println("Valor maximo long" + Long.MAX_VALUE);
        
        /*
            tipo primitvos de tipo flotante: float y double
        */
        
        float numeroFloat = 10.0F;//los unicos con numeros decimales son los double
        float numeroFloat2 = (float)10.0;//Por eso se agrega la F o el float
        System.out.println("numeroFloat2 = " + numeroFloat2);
        System.out.println("Valor minimo float" + Float.MIN_VALUE);
        System.out.println("Valor maximo float" + Float.MAX_VALUE);
    }
}